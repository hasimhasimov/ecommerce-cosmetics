from django.db import models
from django.contrib.auth.models import User
from accounts.models import *
from colorfield.fields import ColorField

# Create your models here.
class Slider(models.Model):
	headline1 = models.CharField(max_length=80)
	description = models.CharField(max_length=255)
	headline2 = models.CharField(max_length=100)
	link = models.URLField()
	image = models.ImageField(upload_to="slider/%Y/%m/%d")

	def __str__(self):
		return self.headline1


class Info(models.Model):
	phone = models.CharField(max_length=25)
	email = models.EmailField(max_length=25)
	address = models.CharField(max_length=255)
	title = models.CharField(max_length=90)
	description = models.CharField(max_length=255)
	about_us = models.TextField()
	terms_of_right = models.TextField()
	legal_notice = models.TextField()


class SocialMedia(models.Model):
	title = models.CharField(max_length=255)
	url = models.URLField()

	def __str__(self):
		return self.title


class Ad(models.Model):
	STATUS_TYPE = (
		('visible', 'Visible'),
		('invisible', 'Invisible')
	)
	title = models.CharField(max_length=100)
	description = models.CharField(max_length=255)
	image = models.ImageField(upload_to="ads/%Y/%m/%d")
	url = models.URLField()
	status = models.CharField(max_length=100, choices=STATUS_TYPE)

	def __str__(self):
		return self.title


class Category(models.Model):
	title = models.CharField(max_length=255)
	image = models.ImageField(upload_to="category/%Y/%m/%d", null=True, blank=True)
	slug = models.SlugField(max_length=255)

	def __str__(self):
		return self.title

class Currency(models.Model):
	title = models.CharField(max_length=10)
	icon = models.CharField(max_length=1)

class Product(models.Model):
	title = models.CharField(max_length=155)
	description = models.CharField(max_length=255)
	category = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True, blank=True)
	content = models.TextField()
	specifications = models.TextField()
	image_main = models.ImageField(upload_to="products/%Y/%m/%d")
	image_2 = models.ImageField(upload_to="products/%Y/%m/%d", null=True, blank=True)
	image_3 = models.ImageField(upload_to="products/%Y/%m/%d", null=True, blank=True)
	image_4 = models.ImageField(upload_to="products/%Y/%m/%d", null=True, blank=True)
	slug = models.SlugField(max_length=255)
	price = models.DecimalField(max_digits=5, decimal_places=2)
	currency = models.ForeignKey(Currency, on_delete=models.SET_NULL, blank=True, null=True)
	has_discount = models.BooleanField()
	discount_rate = models.IntegerField()
	store = models.ForeignKey(Store, on_delete=models.CASCADE)
	is_featured = models.BooleanField(default=False, null=True, blank=True)

	def __str__(self):
		return self.title

class Color(models.Model):
	color = ColorField(default="#FFFFFF")
	product = models.ForeignKey(Product, on_delete=models.CASCADE)

class Review(models.Model):
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	review = models.CharField(max_length=255)
	created_at = models.DateTimeField(auto_now_add=True)
	rating = models.IntegerField()


class WishList(models.Model):
	product = models.ForeignKey(Product, on_delete=models.CASCADE)
	user = models.ForeignKey(User, on_delete=models.CASCADE)

class Cart(models.Model):
	product = models.ForeignKey(Product, on_delete=models.CASCADE)
	quantity = models.IntegerField()
	user = models.ForeignKey(User, on_delete=models.CASCADE)

class Post(models.Model):
	title = models.CharField(max_length=100)
	content = models.TextField()
	image = models.ImageField(upload_to="blog/%Y/%m/%d")
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now_add=True)
	slug = models.SlugField(max_length=155)
	user = models.ForeignKey(User, on_delete=models.CASCADE)

class Comment(models.Model):
	comment = models.CharField(max_length=255)
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	post = models.ForeignKey(Post, on_delete=models.CASCADE)
	created_at = models.DateTimeField(auto_now_add=True)
	is_reply = models.BooleanField()
	reply = models.IntegerField()

class Question(models.Model):
	question = models.CharField(max_length=255)
	answer = models.TextField()


class Partner(models.Model):
	title = models.CharField(max_length=255)
	image = models.ImageField(upload_to="partners/%Y/%m/%d")
	url = models.URLField()


class Order(models.Model):
	user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
	product = models.ForeignKey(Product, on_delete=models.SET_NULL, null=True, blank=True)
	quantity = models.IntegerField()
	order_date = models.DateTimeField(auto_now_add=True)
	is_completed = models.BooleanField(default=False)
	address = models.CharField(max_length=255)