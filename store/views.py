from django.shortcuts import render
from .models import *

# Create your views here.
def home(requests):
	currencies = Currency.objects.all()
	slider = Slider.objects.all()
	categories = Category.objects.all()

	products = Product.objects.all().order_by('-id')[0:40]
	ads = Ad.objects.all().filter(status='visible').order_by('-id')[0:2]

	pro_products = Product.objects.all().filter(is_featured=True).order_by('-id')[0:12]

	posts = Post.objects.all().order_by('-id')[0:6]

	socialMedia = SocialMedia.objects.all()
	info = Info.objects.get(id=1)

	context = {
		'info': info,
		'sm': socialMedia,
		'posts': posts,
		'pro': pro_products,
		'ads': ads,
		'products': products,
		'categories': categories,
		'slider': slider,
		'currencies': currencies,
		'home': True
	}

	return render(requests, "store/home.html", context)