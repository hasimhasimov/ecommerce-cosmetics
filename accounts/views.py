from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.models import User
from django.contrib import messages
from django.db import IntegrityError
from .models import *
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.decorators import login_required
from store.models import Product, Order

# Create your views here.
def register(request):
	if request.method == 'POST':
		name = request.POST['name']
		surname = request.POST['surname']
		email = request.POST['email']
		username = request.POST['username']
		password1 = request.POST['password']
		password2 = request.POST['password2']
		type_ = request.POST['type']

		if password2 == password1:
			if len(password1) > 6:
				try:
					is_exist = User.objects.all().filter(email=email).count()
					if is_exist > 0:
						messages.error(request, "This email has already registered")
						return redirect('register')

					user = User.objects.create_user(username=username, email=email,password=password1, first_name=name, last_name=surname)
					user.save()

					if str(type_) == "0":
						new_profile = Profile()
						new_profile.user = user
						new_profile.save()
					elif str(type_) == "1":
						new_store = Store()
						new_store.user = user
						new_store.save()
					else:
						messages.error(request, "Invalid data")
						return redirect('register')

					messages.success(request, "You are registered successfully.")
					return redirect('login')
				except IntegrityError as i:
					messages.error(request, "This username has already taken")
			else:
				messages.error(request, "Password should be at least 6 characters")
		else:
			messages.error(request, "Passwords should be the same")

		return redirect('register')


	return render(request, "accounts/register.html")


def login_user(request):
	if request.method == 'POST':
		username = request.POST['username']
		password = request.POST['password']

		user = authenticate(request, username=username, password=password)
		if user:
			login(request, user)
			messages.success(request, "You are successfully logged in!")
			return redirect('home')
		else:
			messages.warning(request, "There is not such user.")
			return redirect('login')

	return render(request, 'accounts/login.html')


@login_required
def account(request):
	has_profile  = Profile.objects.all().filter(user=request.user).count()
	if has_profile == 0:
		has_store = Store.objects.all().filter(user=request.user).count()
		if has_store > 0:
			store = Store.objects.get(user=request.user)
			products = Product.objects.all().filter(store=store).order_by('-id')
			orders = Order.objects.all().filter(product_id=store.id).order_by('-id')
			context = {
				'store': store,
				'products': products,
				'orders': orders
			}
			return render(request, "accounts/store_account.html", context)
	else:
		profile = Profile.objects.get(user=request.user)
		context = {
			'profile': profile
		}
		return render(request, "accounts/user_account.html", context)