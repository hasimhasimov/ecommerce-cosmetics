from django.db import models
from django.contrib.auth.models import User

class Profile(models.Model):
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	image = models.ImageField(upload_to="user/%Y/%m/%d")
	phone = models.CharField(max_length=25)
	address = models.CharField(max_length=255)
	post_code = models.CharField(max_length=10)


class Store(models.Model):
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	title = models.CharField(max_length=255)
	description = models.CharField(max_length=255)
	image = models.ImageField(upload_to="store/%Y/%m/%d")
	slug = models.SlugField(max_length=255)